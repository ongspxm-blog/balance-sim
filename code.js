const game = {
    _fps: 60,
    _tstamp: null,
    _running: false,
    _status: {
        rotation: 0,
        velocity: 0,
        position: 0.5
    },
    _engine: {
        gravity: 10,
        rot_max: 0.3,
        mass_ball: 1,
        dist_motor: 0.1,
    }
};

function _tstamp() {
    return (new Date()).getTime();
}

function game_update(dt) {
    range = 0.2;
    game._status.position += ((Math.random()-0.5)*range)*dt;

    const rot = game._status.rotation * Math.PI;
    const f_g = game._engine.gravity * game._engine.mass_ball;
    const f_v = Math.sin(rot) * f_g;
    const f_m = Math.abs(Math.cos(rot) * f_g);

    const m = f_m * (game._status.position-0.5) * 0.01;
    game._status.rotation += m;
    game._status.rotation = Math.max(Math.min(
        game._status.rotation, game._engine.rot_max),
        -1*game._engine.rot_max);
    document.getElementById('status_misc').innerHTML = `${f_v} ${m}`;
}

function game_display() {
    document.getElementById('status_tstamp').innerHTML = game._tstamp;
    document.getElementById('status_pos').innerHTML = game._status.position.toFixed(4);
    document.getElementById('status_rot').innerHTML = game._status.rotation.toFixed(4);

    document.getElementById('bar').style.transform = `rotate(${game._status.rotation*90}deg)`;
    document.getElementById('indi_pos').style.width = parseInt(game._status.position*100)+'%';
}

function game_loop() {
    var updated = 0;
    const tstamp = _tstamp();
    while (game._tstamp < tstamp) {
        game._tstamp += parseInt(1000/game._fps);
        game_update(1/game._fps);
        updated += 1;
    }
    if (updated) game_display();

    // NOTE: this is a hack for the dom to update
    // if the function is not exited, the dom wont refresh
    // happens on both ff and chrome
    if (game._running) setTimeout(game_loop, 1);
}

function game_start() {
    while (game._running) {
        game._running = false;
    }

    game._running = true;
    game._tstamp = _tstamp();
    game_loop();
}
