/** considering a 1 meter rod **/
const game = {
    fps: 60,
    gravity: 10,
    rotation: 0,

    // maximum range of rotation
    m_rot: Math.PI/6,

    pos_v: 0,
    pos_s: 0.5,

    voltage: 0,
    voltage_var: 0.0001,

    m_ball: 0.2,
    m_motor: 1,
    r_motor: 0.2,

    torque_maxN: 100,
    torque_dist: 0.05,

    hist_s: [],
    hist_v: [],

    // using rubber on dry concrete
    fric_s: 0.5,
    fric_k: 0.3,
    fric_mthres: 0.001,

    timeout: null,
    isrunning: false,

    stats_hist: 1,
};

function init() {
    clearTimeout(game.timeout);
    game.goal = document.getElementById("params_goal").value;
    const gtime = 1/10;
    const gtune = gettuning(game.goal);
    document.getElementById("d_kp").textContent = gtune[0].toFixed(4);
    document.getElementById("d_ki").textContent = gtune[1].toFixed(4);
    document.getElementById("d_kd").textContent = gtune[2].toFixed(4);
    document.getElementById("p_ki").textContent = gtime.toFixed(4);
    game.pid = PID(gtune[0],gtune[1],gtune[2],game.fps*gtime);

    if (!game.svg) {
        const g_W=800, g_H=300;
        const g_margs = [10, 30, 30, 60],
            g_w = g_W-g_margs[1]-g_margs[3],
            g_h = g_H-g_margs[0]-g_margs[2];
        const svg = d3.select("#graph")
            .append("svg")
                .attr("width", g_W).attr("height", g_H)
            .append("g")
                .attr("transform", "translate("+g_margs[3]+","+g_margs[0]+")");

        const axisy = d3.scaleLinear()
            .domain([0,100]).range([g_h,0]);
        svg.append("g").call(d3.axisLeft(axisy));

        const axisx = d3.scaleLinear()
            .domain([0,100]).range([0,g_w]);
        game.axis_x = svg.append("g")
            .attr("transform", "translate(0,"+g_h+")");
        game.axis_x.call(d3.axisBottom(axisx));

        svg.append("path").attr("class", "pos_s");
        svg.append("path").attr("class", "pos_v");

        game.svg = svg;
        game.axisx = axisx;
        game.g_w = g_w;
        game.g_h = g_h;
    }

    game.isrunning = true;
    schedule();
}
window.onkeypress = function(e) {
    var del = 0;
    if (e.key == 'd') del = 1;
    if (e.key == 'a') del = -1;
    if (e.key == 's') game.isrunning=false;
    if (del) rotate(del*0.1);
};

function rotate(del) {
    // -1 < del < 1, as compared to game.torque_maxN
    game.voltage += del;
    game.voltage = Math.max(Math.min(game.voltage,1),-1);
}

function PID(kp=0, ki=0, kd=0, datacnt=game.fps) {
    var history = [];

    function update(curr, ideal=0.5) {
        history.push(curr);
        if (history.length > datacnt) history.shift();
        var di = 0;
        for (var i=0; i<history.length; i++) di+=(ideal-history[i]);
        var dd = history[history.length-1]-history[history.length-2] || 0;

        const res = kp*(ideal-curr) + ki*di + kd*dd;
        return res;
    }

    return { update };
}

const tunings = {
    00: [0.6,0,0],
    // 25: [0.0005,0.001,0.2],
    // 40: [0.0006,0.001,0.3],
};
function gettuning(goal) {
    const k = 1;
    const t = 0.198;
    return [0.5*k, 0.5, 0];
    return [k,0,0];
    return [0.6*k, 1.2*k/t, 0.075*k*t];
    const diff = parseInt(Math.abs(goal*100-50));

    const tunings2 = [];
    for (v in tunings) {
        tunings2.push([Math.abs(diff-v), tunings[v]]);
    }
    tunings2.sort((a,b)=>a[0]>b[0]);

    const diffa = tunings2[0];
    const diffb = tunings2[1];

    const ratioa = diffb[0]/(diffa[0]+diffb[0]);
    const ratiob = diffa[0]/(diffa[0]+diffb[0]);

    return [0,1,2].map(i=>ratioa*diffa[1][i]+ratiob*diffb[1][i]);
}

function botmove(ideal=0.5) {
    game.voltage = game.pid.update(game.pos_s, ideal);
}

function schedule() {
    const dt = 1/game.fps;
    game.stime = new Date();

    while (true) {
        const stime = new Date();
        while (stime > game.stime+dt) {
            game.stime += dt;
            update(dt);
        }
        render(dt);
        if (!game.isrunning) break;
    }
}

function update(dt=1/game.fps) {
    // update beam rotation
    const d_motor = game.torque_dist;
    const f_motor = game.voltage * game.torque_maxN;

    const d_ball = Math.abs(game.pos_s - 0.5);
    const f_ball = game.m_ball*game.gravity*Math.sin(game.rotation);

    // angular moment of the bar, based on motor and ball pos
    const sum_torque = (f_ball*d_ball)+(2*f_motor*d_motor);
    const sum_inertia = game.m_ball*d_ball + 0.5*game.m_motor*(game.r_motor**2);
    game.rotation += (sum_torque/sum_inertia)*dt;

    game.rotation = Math.max(Math.min(
        game.m_rot, game.rotation), -1*game.m_rot);

    // changing pos_v based on friction
    const ad = Math.sin(game.rotation)*game.gravity;
    const an = Math.abs(Math.cos(game.rotation)*game.gravity);

    var ismoving = true;
    if (Math.abs(game.pos_v*game.m_ball) < game.fric_mthres) {
        // must have inertia = minimum momentum
        ismoving = Math.abs(ad)>Math.abs(an*game.fric_s);
    }

    if (ismoving) {
        // fric always opposite direction of motion
        game.pos_v += ad*dt;
        var dir = 1-2*(game.pos_v < 0);

        game.pos_v += dir*-1*(an*game.fric_k)*dt;
        var dir2 = 1-2*(game.pos_v < 0);

        // edge case: friction completely stops motion
        if (dir!=dir2) game.pos_v = 0;
    } else { game.pos_v = 0; }

    // update position
    game.pos_s += game.pos_v;
    if (game.pos_s < 0 || game.pos_s > 1) {
        game.pos_s = Math.min(Math.max(0,game.pos_s),1);
        game.pos_v = 0;
    }

    botmove(game.goal);
    game.voltage += (Math.random() - 0.5) * game.voltage_var;

    // stats
    game.hist_s.push(game.pos_s);
    game.hist_v.push((game.voltage*0.5)+0.5);

    game.axisx.domain([game.hist_s.length*dt,0]);
    game.axis_x.call(d3.axisBottom(game.axisx));
}

function render(dt) {
    // drawing
    const bar = document.getElementById("bar");
    const inner = document.getElementById("inner");
    const angle = game.rotation*180/Math.PI;
    bar.style.transform = "rotate("+angle+"deg)";
    inner.style.width = (game.pos_s*100)+"%";

    const diff = Math.abs(game.goal-game.pos_s);
    var color = "#0c0";
    if (diff>0.05) color="#cc0";
    if (diff>0.1) color="#c00";
    inner.style.borderColor = color;

    // update graphs
    const hleng = dt*game.stats_hist;
    const hists = game.hist_s.slice(Math.max(0,game.hist_s.length-hleng));
    const appnd = "&nbsp;("+game.stats_hist+"s)";

    function histfunc(fn) {
        return hists.reduce(fn,0)/hists.length;
    }

    const stat_avg = histfunc((a,b)=>a+b);
    const stat_var = Math.pow(histfunc((a,b)=>a+Math.pow(b-stat_avg,2)),0.5);

    document.getElementById("pavg").innerHTML = stat_avg.toFixed(4);
    document.getElementById("pvar").innerHTML = stat_var.toFixed(4) + appnd;

    const line = d3.line()
        .x(function(d,i){ return (1-(i/game.hist_s.length))*game.g_w; })
        .y(function(d){ return (1-d)*game.g_h; });
    game.svg.select(".pos_s").attr("d", line(game.hist_s));
    game.svg.select(".pos_v").attr("d", line(game.hist_v));
}
